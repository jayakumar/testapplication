package com.example.testingapplication

import org.junit.runner.RunWith
import org.junit.runners.Suite

/**
 * Created by Jinu on 10/11/18.
 **/

@RunWith(Suite::class)
@Suite.SuiteClasses (LoginUnitTest::class, SignUpUnitTest::class,HomeUnitTest::class)
class RunAllTests