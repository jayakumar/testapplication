package com.example.testingapplication

import com.example.testingapplication.api.CountryData
import com.example.testingapplication.home.HomeContract
import com.example.testingapplication.home.HomePresenter
import com.example.testingapplication.mock.HomeRepo
import com.google.gson.Gson
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import java.io.File
import java.io.FileReader

/**
 * Created by Jinu on 10/11/18.
 **/

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@RunWith(MockitoJUnitRunner::class)
class HomeUnitTest {

    @Mock
    @JvmField
    var view: HomeContract.View? = null

    private lateinit var presenter: HomePresenter

    @Before
    fun onBefore() {
        presenter = HomePresenter(view, HomeRepo())
    }


    @Test
    fun testApiError() {
        Connection.status = Connection.DISCONNECTED
        presenter.loadCountryData(0)
        verify(view)?.showError("No Connection error")
    }

    @Test
    fun testSuccess() {
        Connection.status = Connection.CONNECTED
        presenter.loadCountryData(0)
        verify(view, never())?.showError("An error occurred")
        val file = File(this.javaClass.classLoader.getResource("country.json").path)
        val reader = FileReader(file)
        val buffer = StringBuffer()
        reader.forEachLine {
            buffer.append(it)
        }
        verify(view)?.showItem(Gson().fromJson(buffer.toString(), CountryData::class.java))
    }


    @After
    fun onAfter() {
        reset(view)
    }
}