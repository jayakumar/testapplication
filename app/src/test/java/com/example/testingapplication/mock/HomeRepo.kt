package com.example.testingapplication.mock

import com.example.testingapplication.Connection
import com.example.testingapplication.api.BaseDataSource
import com.example.testingapplication.api.CountryData
import com.example.testingapplication.home.HomeContract
import com.google.gson.Gson
import java.io.File
import java.io.FileReader


/**
 * Created by Jinu on 10/11/18.
 **/
class HomeRepo :HomeContract.DataSource{

    override fun getCountry(pos: Int, task: BaseDataSource.Task<CountryData>) {
        if (Connection.status==Connection.DISCONNECTED){
            task.onError("No Connection error")
        }else{

            val file = File(this.javaClass.classLoader.getResource("country.json").path)
            val reader=FileReader(file)
            val buffer=StringBuffer()
            reader.forEachLine {
                buffer.append(it)
            }
            task.onResponse(Gson().fromJson(buffer.toString(),CountryData::class.java))
        }
    }
}