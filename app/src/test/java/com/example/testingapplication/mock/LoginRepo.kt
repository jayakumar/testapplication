package com.example.testingapplication.mock

import com.example.testingapplication.api.BaseDataSource
import com.example.testingapplication.login.LoginContract
import com.example.testingapplication.utils.TextUtils
import java.util.regex.Pattern

class LoginRepo : LoginContract.DataSource {


    override fun login(username: String, password: String, task: BaseDataSource.Task<String>) {
        if (!TextUtils.validate(username)) {
            task.onError("Email is not valid")
            return
        }

        task.onResponse("Login success")

    }

}