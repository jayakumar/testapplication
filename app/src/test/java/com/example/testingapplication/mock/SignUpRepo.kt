package com.example.testingapplication.mock

import com.example.testingapplication.api.BaseDataSource
import com.example.testingapplication.signup.SignupContract
import com.example.testingapplication.utils.TextUtils

/**
 * Created by Jinu on 10/10/18.
 **/
class SignUpRepo:SignupContract.DataSource {

    override fun signUp(name: String, email: String, password: String, task: BaseDataSource.Task<String>) {
        if (!TextUtils.validate(email)) {
            task.onError("Email id is not valid")
            return
        }
        task.onResponse("Sign up success.")
    }
}