package com.example.testingapplication

import com.example.testingapplication.mock.SignUpRepo
import com.example.testingapplication.signup.SignUpPresenter
import com.example.testingapplication.signup.SignupContract
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.never
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class SignUpUnitTest {

    @Mock
    @JvmField
    var view: SignupContract.View? = null

    private lateinit var presenter: SignUpPresenter

    @Before
    fun onBefore() {
        presenter = SignUpPresenter(view, SignUpRepo())
    }

    @Test
    fun checkUsernameError() {
        presenter.signUp(null,"lkflkjdsflds","kldsjfldsflds")
        verify(view)?.showError("Name cannot be empty",SignupContract.View.Type.TYPE_NAME)
        verify(view, never())?.showError("Email cannot be empty",SignupContract.View.Type.TYPE_EMAIL)
        verify(view, never())?.showError("Password cannot be empty",SignupContract.View.Type.TYPE_PASSWORD)
        verify(view, never())?.onSignUpError("Email id is not valid")
        verify(view, never())?.onSuccessSignUp("Sign up success.")
    }

    @Test
    fun checkEmailError() {
        presenter.signUp("dkfjdskfsd",null,"kldsjfldsflds")
        verify(view, never())?.showError("Name cannot be empty",SignupContract.View.Type.TYPE_NAME)
        verify(view)?.showError("Email cannot be empty",SignupContract.View.Type.TYPE_EMAIL)
        verify(view, never())?.showError("Password cannot be empty",SignupContract.View.Type.TYPE_PASSWORD)
        verify(view, never())?.onSignUpError("Email id is not valid")
        verify(view, never())?.onSuccessSignUp("Sign up success.")
    }

    @Test
    fun checkPasswordError() {
        presenter.signUp("dsflkdsfksd","lksdfjdksjfds",null)
        verify(view, never())?.showError("Name cannot be empty",SignupContract.View.Type.TYPE_NAME)
        verify(view, never())?.showError("Email cannot be empty",SignupContract.View.Type.TYPE_EMAIL)
        verify(view)?.showError("Password cannot be empty",SignupContract.View.Type.TYPE_PASSWORD)
        verify(view, never())?.onSignUpError("Email id is not valid")
        verify(view, never())?.onSuccessSignUp("Sign up success.")
    }

    @Test
    fun userNamePwdError(){
        presenter.signUp(null,"lksdfjdksjfds",null)
        verify(view)?.showError("Name cannot be empty",SignupContract.View.Type.TYPE_NAME)
        verify(view, never())?.showError("Email cannot be empty",SignupContract.View.Type.TYPE_EMAIL)
        verify(view)?.showError("Password cannot be empty",SignupContract.View.Type.TYPE_PASSWORD)
        verify(view, never())?.onSignUpError("Email id is not valid")
        verify(view, never())?.onSuccessSignUp("Sign up success.")
    }


    @Test
    fun userNameEmailError(){
        presenter.signUp(null,null,"ksdfkljsdjlfksd")
        verify(view)?.showError("Name cannot be empty",SignupContract.View.Type.TYPE_NAME)
        verify(view)?.showError("Email cannot be empty",SignupContract.View.Type.TYPE_EMAIL)
        verify(view, never())?.showError("Password cannot be empty",SignupContract.View.Type.TYPE_PASSWORD)
        verify(view, never())?.onSignUpError("Email id is not valid")
        verify(view, never())?.onSuccessSignUp("Sign up success.")
    }

    @Test
    fun userPasswordEmailError(){
        presenter.signUp("kjsdlfhlsdflsdfsld",null,null)
        verify(view, never())?.showError("Name cannot be empty",SignupContract.View.Type.TYPE_NAME)
        verify(view)?.showError("Email cannot be empty",SignupContract.View.Type.TYPE_EMAIL)
        verify(view)?.showError("Password cannot be empty",SignupContract.View.Type.TYPE_PASSWORD)
        verify(view, never())?.onSignUpError("Email id is not valid")
        verify(view, never())?.onSuccessSignUp("Sign up success.")
    }

    @Test
    fun signUpFailure(){
        presenter.signUp("kjsdlfhlsdflsdfsld","dkjfhdskjfhsd","jkdshfdsjfhksd")
        verify(view, never())?.showError("Name cannot be empty",SignupContract.View.Type.TYPE_NAME)
        verify(view, never())?.showError("Email cannot be empty",SignupContract.View.Type.TYPE_EMAIL)
        verify(view, never())?.showError("Password cannot be empty",SignupContract.View.Type.TYPE_PASSWORD)
        verify(view)?.onSignUpError("Email id is not valid")
        verify(view, never())?.onSuccessSignUp("Sign up success.")
    }

    @Test
    fun signUpSuccess(){
        presenter.signUp("kjsdlfhlsdflsdfsld","jinu@gmail.com","jkdshfdsjfhksd")
        verify(view, never())?.showError("Name cannot be empty",SignupContract.View.Type.TYPE_NAME)
        verify(view, never())?.showError("Email cannot be empty",SignupContract.View.Type.TYPE_EMAIL)
        verify(view, never())?.showError("Password cannot be empty",SignupContract.View.Type.TYPE_PASSWORD)
        verify(view)?.onSuccessSignUp("Sign up success.")
        verify(view, never())?.onSignUpError("Email id is not valid")
    }

    @After
    fun onAfter() {

    }
}