package com.example.testingapplication.facade.test

/**
 * Created by Jinu on 10/23/18.
 */
interface Shape{
    fun draw()
}
