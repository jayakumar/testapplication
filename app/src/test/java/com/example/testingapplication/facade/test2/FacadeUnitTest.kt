package com.example.testingapplication.facade.test2

import com.example.testingapplication.facade.data.OrderFacade
import org.junit.Test

/**
 * Created by Jinu on 10/22/18.
 **/
class FacadeUnitTest {

    @Test
    fun checkSomething() {
        val handler= TransactionHandler(123456789, 123)
        handler.withdrawMoney(1500.0)
        handler.depositMoney(200.0)
    }

    @Test
    fun checkFacade(){
        val orderFacade = OrderFacade()
        orderFacade.placeOrder("OR123456")
        println("Order processing completed")
    }
}