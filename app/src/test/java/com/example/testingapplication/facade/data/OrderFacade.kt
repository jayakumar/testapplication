package com.example.testingapplication.facade.data

/**
 * Created by Jinu on 10/22/18.
 **/
class OrderFacade {
    private val pymt = Payment()
    private val inventry = Inventory()

    fun placeOrder(orderId: String) {
        val step1 = inventry.checkInventory(orderId)
        val step2 = pymt.deductPayment(orderId)
        println("Following steps completed:$step1 & $step2")
    }

}