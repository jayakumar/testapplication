package com.example.testingapplication.facade.test2

/**
 * Created by Jinu on 10/22/18.
 **/
class Withdraw : Bank {

    override fun transaction(amount: Double) {
        if (amount > checker.getTotalAmount()) {
            println("Sorry, You cannot have enough money to withdraw")
            System.out.println("Current Balance: " + checker.getTotalAmount() + "\n")
        } else {
            checker.totAmount -= amount
            println("Withdrawn is completed: $amount")
            System.out.println("Current Balance: " + checker.getTotalAmount() + "\n")
        }
    }

}