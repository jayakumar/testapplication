package com.example.testingapplication.facade.test2

/**
 * Created by Jinu on 10/22/18.
 **/
class TransactionHandler(accNo: Int, secNo: Int) {

    private var accountNo = 123456789
    private var securityNo = 123

    internal var withdraw: Withdraw
    internal var deposit: Deposit

    internal var accChecker: CheckAccount

    init {
        accountNo = accNo
        securityNo = secNo
        withdraw = Withdraw()
        deposit = Deposit()
        accChecker = CheckAccount()

    }

    fun withdrawMoney(withdrawCash: Double) {
        if (accountNo == accChecker.getAccountNumber() && securityNo == accChecker.getSecurityNumber()) {
            println("Access Granted")
            withdraw.transaction(withdrawCash)
        } else {
            println("Access Dinied: invalid credentials! \n")
        }
    }

    fun depositMoney(withdrawCash: Double) {
        if (accountNo == accChecker.getAccountNumber() && securityNo == accChecker.getSecurityNumber()) {
            println("Access Granted")
            deposit.transaction(withdrawCash)
        } else {
            println("Access Dinied: invalid credentials! \n")
        }
    }
}