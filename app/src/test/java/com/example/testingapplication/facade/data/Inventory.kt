package com.example.testingapplication.facade.data

/**
 * Created by Jinu on 10/22/18.
 **/
class Inventory {

    fun checkInventory(OrderId: String): String {
        return "Inventory checked $OrderId"
    }

}