package com.example.testingapplication.facade.test

/**
 * Created by Jinu on 10/23/18.
 **/
class Rectagle :Shape {

    override fun draw() {
        println("Created a rectangle shape")
    }
}