package com.example.testingapplication.facade.test2

/**
 * Created by Jinu on 10/22/18.
 **/
interface Bank {

    var checker: CheckAccount
        get() = CheckAccount()
        set(value) = TODO()

    fun transaction(amount: Double)

}