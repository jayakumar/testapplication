package com.example.testingapplication.facade.data

/**
 * Created by Jinu on 10/22/18.
 **/
 class Payment {
    fun deductPayment(orderID: String): String {
        return "Payment deducted successfully $orderID"
    }
}
