package com.example.testingapplication.facade.test

/**
 * Created by Jinu on 10/23/18.
 **/
class ShapeMaker() {

    private var circle: Shape = Circle()
    private var rectangle: Shape = Rectagle()
    private var roundedRect: Shape = RoundedRect()

    fun drawCircle() {
        circle.draw()
    }

    fun drawRectangle() {
        rectangle.draw()
    }

    fun drawRoundRect() {
        roundedRect.draw()
    }

}