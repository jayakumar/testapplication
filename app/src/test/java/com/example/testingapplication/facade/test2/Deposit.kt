package com.example.testingapplication.facade.test2

/**
 * Created by Jinu on 10/22/18.
 **/
class Deposit : Bank {

    override fun transaction(amount: Double) {
        checker.totAmount += amount
        println("Deposit is completed: $amount")
        System.out.println("Current Balance: " + checker.getTotalAmount() + "\n")
    }

}