package com.example.testingapplication.facade.test2

/**
 * Created by Jinu on 10/22/18.
 **/
class CheckAccount {

    private val accountNo = 123456789
    private val securityNo = 123

    var totAmount = 1000.0

    fun getTotalAmount(): Double {
        return totAmount
    }

    fun getAccountNumber(): Int {
        return accountNo
    }

    fun getSecurityNumber(): Int {
        return securityNo
    }
}