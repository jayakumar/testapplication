package com.example.testingapplication.api

import android.os.Parcel
import android.os.Parcelable

data class CountryData(var country: List<CountryItem>) : Parcelable {
    constructor(source: Parcel) : this(
            source.createTypedArrayList(CountryItem.CREATOR)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeTypedList(country)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CountryData> = object : Parcelable.Creator<CountryData> {
            override fun createFromParcel(source: Parcel): CountryData = CountryData(source)
            override fun newArray(size: Int): Array<CountryData?> = arrayOfNulls(size)
        }
    }
}