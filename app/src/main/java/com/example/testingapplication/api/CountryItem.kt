package com.example.testingapplication.api

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class CountryItem(@SerializedName("FIELD4") var FIELD4: String? = null,
                       @SerializedName("idPrefix") var idPrefix: Int = 0,
                       @SerializedName("countryName") var countryName: String? = null,
                       @SerializedName("FIELD5") var FIELD5: Int = 0) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readInt(),
            source.readString(),
            source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(FIELD4)
        writeInt(idPrefix)
        writeString(countryName)
        writeInt(FIELD5)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CountryItem> = object : Parcelable.Creator<CountryItem> {
            override fun createFromParcel(source: Parcel): CountryItem = CountryItem(source)
            override fun newArray(size: Int): Array<CountryItem?> = arrayOfNulls(size)
        }
    }
}
