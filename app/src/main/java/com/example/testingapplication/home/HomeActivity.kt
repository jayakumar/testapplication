package com.example.testingapplication.home

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.cycloides.paginationlibrary.PaginationAdapter
import com.example.testingapplication.R
import com.example.testingapplication.api.CountryData
import com.example.testingapplication.api.CountryItem
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() ,HomeContract.View ,PaginationAdapter.PaginationListener{

    private lateinit var presenter: HomePresenter
    private lateinit var adapter: HomeAdapter
    private var list=ArrayList<CountryItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        presenter=HomePresenter(this,HomeRepository())
        adapter= HomeAdapter(list,this)
        recyclerView.layoutManager=LinearLayoutManager(this)
        recyclerView.adapter=adapter
        onLoadMore(0)
    }

    override fun onLoadMore(pos: Int) {
        presenter.loadCountryData(pos)
    }

    override fun showItem(countryData: CountryData) {
        progressBar.visibility= View.GONE
        list.addAll(countryData.country)
        adapter.showNewItems()
    }

    override fun showError(errorMessage: String) {
        progressBar.visibility= View.GONE
        Snackbar.make(recyclerView,errorMessage,Snackbar.LENGTH_SHORT).show()
    }
}
