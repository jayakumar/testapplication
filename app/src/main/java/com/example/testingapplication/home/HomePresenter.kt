package com.example.testingapplication.home

import com.example.testingapplication.api.BaseDataSource
import com.example.testingapplication.api.CountryData


/**
 * Created by Jinu on 10/11/18.
 **/
class HomePresenter(private var view: HomeContract.View?,
                    private var dataSource: HomeContract.DataSource) :HomeContract.Presenter {

    override fun loadCountryData(pos: Int) {
        dataSource.getCountry(pos,object :BaseDataSource.Task<CountryData>{
            override fun onResponse(response: CountryData) {
                view?.showItem(response)
            }

            override fun onError(error: String) {
                view?.showError(error)
            }
        })
    }
}