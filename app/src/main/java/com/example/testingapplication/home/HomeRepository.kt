package com.example.testingapplication.home

import com.example.testingapplication.api.ApiProvider
import com.example.testingapplication.api.BaseDataSource
import com.example.testingapplication.api.CountryData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Jinu on 10/11/18.
 **/
class HomeRepository :HomeContract.DataSource {

    override fun getCountry(pos: Int, task: BaseDataSource.Task<CountryData>) {
        ApiProvider.apiResponse.getList().enqueue(object : Callback<CountryData> {
            override fun onFailure(call: Call<CountryData>, t: Throwable) {
               task.onError(t.localizedMessage)
            }

            override fun onResponse(call: Call<CountryData>, response: Response<CountryData>) {
                if (response.isSuccessful && response.body()!=null) {
                    task.onResponse(response.body()!!)
                }else{
                    task.onError("An error occurred")
                }
            }

        })
    }
}