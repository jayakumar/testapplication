package com.example.testingapplication.home

import com.example.testingapplication.api.BaseDataSource
import com.example.testingapplication.api.CountryData

/**
 * Created by Jinu on 10/11/18.
 **/
interface HomeContract {

    interface View {

        fun showItem(countryData: CountryData)

        fun showError(errorMessage: String)

    }

    interface Presenter {

        fun loadCountryData(pos: Int)

    }

    interface DataSource : BaseDataSource {

        fun getCountry(pos: Int, task: BaseDataSource.Task<CountryData>)

    }
}