package com.example.testingapplication.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.cycloides.paginationlibrary.PaginationAdapter
import com.example.testingapplication.R
import com.example.testingapplication.api.CountryItem

/**
 * Created by Jinu on 10/11/18.
 **/
class HomeAdapter(list: ArrayList<CountryItem>, listener: PaginationListener) : PaginationAdapter<HomeAdapter.HomeVH, CountryItem>(list, listener) {


    override fun onCreateVH(parent: ViewGroup, viewType: Int): HomeVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.country_item,parent,false)
        return HomeVH(view)
    }

    override fun onBindVH(holder: HomeVH, position: Int, item: CountryItem) {
        holder.countryName.text = item.countryName
        holder.prefix.text = "+${item.idPrefix}"
    }


    class HomeVH(view: View) : RecyclerView.ViewHolder(view) {
        var countryName: TextView = view.findViewById(R.id.countryName)
        var prefix: TextView = view.findViewById(R.id.prefix)
    }
}