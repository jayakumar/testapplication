package com.example.testingapplication.login

import android.os.Handler
import com.example.testingapplication.api.BaseDataSource
import com.example.testingapplication.utils.TextUtils
import java.util.regex.Pattern

class LoginRepository : LoginContract.DataSource {



    override fun login(username: String, password: String, task: BaseDataSource.Task<String>) {
        if (!TextUtils.validate(username)) {
            task.onError("Email is not valid")
            return
        }
        Handler().post {
            task.onResponse("Login success")
        }
    }



}