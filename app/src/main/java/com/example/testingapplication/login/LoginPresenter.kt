package com.example.testingapplication.login

import com.example.testingapplication.api.BaseDataSource
import com.example.testingapplication.utils.TextUtils

class LoginPresenter(var view: LoginContract.View?, private var dataSource: LoginContract.DataSource) : LoginContract.Presenter {


    override fun login(username: String?, password: String?) {
        if (isDataValid(username, password)) return
        dataSource.login(username!!, password!!, task = object : BaseDataSource.Task<String> {
            override fun onResponse(response: String) {
                view?.showSuccess(response)
                view?.launchHomeScreen()
            }

            override fun onError(error: String) {
                view?.showError(error)
            }
        })
    }

    private fun isDataValid(username: String?, password: String?): Boolean {
        if (TextUtils.isEmpty(username)) {
            view?.showError("Username is empty", LoginContract.LoginType.USER_NAME)
        }

        if (TextUtils.isEmpty(password)) {
            view?.showError("Password is empty", LoginContract.LoginType.PASSWORD)
        }

        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            return true
        }
        view?.clearError()
        return false
    }


}