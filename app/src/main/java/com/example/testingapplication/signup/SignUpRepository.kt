package com.example.testingapplication.signup

import android.os.Handler
import com.example.testingapplication.api.BaseDataSource
import com.example.testingapplication.utils.TextUtils
import java.util.regex.Pattern

class SignUpRepository : SignupContract.DataSource {



    override fun signUp(name: String, email: String, password: String, task: BaseDataSource.Task<String>) {

        if (!TextUtils.validate(email)) {
            task.onError("Email id is not valid")
            return
        }

        Handler().post {
            task.onResponse("Sign up success.")
        }
    }
}