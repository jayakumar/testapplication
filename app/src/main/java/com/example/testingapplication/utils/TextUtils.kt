package com.example.testingapplication.utils

import java.util.regex.Pattern

/**
 * Created by Jinu on 10/10/18.
 */
object TextUtils {

    private val validEmailAddressRegex = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE)

    fun isEmpty(text: String?): Boolean {
        return text == null || text.isEmpty()
    }

    fun validate(email: String): Boolean {
        val matcher = validEmailAddressRegex.matcher(email)
        return matcher.find()
    }


}
