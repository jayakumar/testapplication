package com.cycloides.paginationlibrary

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView


import java.util.ArrayList

/**
 * this class will help you to create pagination functionality
 *
 *
 * in here VH is your view holder and D is your data type
 *
 *
 *
 *
 * @param <D> is the type of list item
</D> */
@Suppress("MemberVisibilityCanBePrivate", "UNUSED_PARAMETER", "UNCHECKED_CAST")
abstract class PaginationAdapter<VH : RecyclerView.ViewHolder, D>
/**
 * @param list     of items you have
 * @param mListener interface to load more items
 */
(private val list: ArrayList<D>?, private val mListener: PaginationListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var isLoading = true
    private var isFinishedLoading = false
    private val typeLoading = -100
    private var previousSize = 0

    var recyclerView: RecyclerView? = null

    /**
     * to notify adapter about the pagination
     *
     * fetching new content else false
     */
    private fun setLoading() {
        isLoading = true
        previousSize = list!!.size
    }

    fun showNewItems() {
        isLoading = false
        notifyItemRangeInserted(previousSize, list!!.size - previousSize)
        previousSize = list.size
    }


    /**
     * once all data is loaded and needs no more pagination call this method
     */
    fun setFinishedLoading() {
        isFinishedLoading = true
        isLoading = true
        notifyItemRemoved(list!!.size + 1)
    }

    /**
     * interface for loading new items to the list
     * once list reaches to the end this method will be called
     */
    interface PaginationListener {

        /**
         * called once reaches the end of the list
         *
         * @param pos last item pos
         */
        fun onLoadMore(pos: Int)
    }

    /**
     * method to get data for the list item
     *
     * @param pos position
     * @return data
     */
    protected fun getItem(pos: Int): D {
        return list!![pos]
    }

    /**
     * Same like it's on [.getItemViewType] on recycler view adapter
     *
     * ---------------------------
     * note : don't return -100 as a view type cuz we are using that for list item
     * ---------------------------
     */
    protected fun getItemViewTypes(pos: Int): Int {
        return 0
    }


    /**
     * same as  [.onCreateViewHolder] on recycler view adapter
     */
    protected abstract fun onCreateVH(parent: ViewGroup, viewType: Int): VH

    /**
     * same as [.onBindViewHolder] on recycler view adapter
     *
     * and btw
     * @param item is your item type for the position
     */
    protected abstract fun onBindVH(holder: VH, position: Int, item: D)

    final override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == typeLoading) {
            return onCreateLoadingVH(parent)
        }
        return onCreateVH(parent, viewType)
    }

    /**
     * use this method to customize loading ViewHolder
     * */
    protected open fun onCreateLoadingVH(parent: ViewGroup): LoadingVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.loading_item, parent, false)
        return LoadingVH(view)
    }

    final override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoadingVH) {
            executeLoadMore(position)
        } else {
            onBindVH(holder as VH, position, getItem(position))
        }
    }

    private fun executeLoadMore(position: Int) {
        Log.d(this@PaginationAdapter.javaClass.name,"position $position")
        if (!isLoading) {
            mListener.onLoadMore(position)
            setLoading()
        }
    }

    final override fun getItemViewType(position: Int): Int {
        return if (position >= list!!.size) {
            typeLoading
        } else getItemViewTypes(position)
    }

    final override fun getItemCount(): Int {
        if (list == null) {
            return 0
        }
        if (list.isEmpty()) {
            return 0
        }
        return if (isFinishedLoading) list.size else list.size + 1
    }

    /**
     * A base class for loading viewHolder
     * */
    open class LoadingVH internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView)


}
