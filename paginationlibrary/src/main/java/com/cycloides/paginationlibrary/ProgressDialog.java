package com.cycloides.paginationlibrary;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatDialog;

/**
 * Created by Jinu on 24/5/18.
 **/
public class ProgressDialog extends AppCompatDialog {

    private ImageView mProgressCircle;

    public ProgressDialog(Context context) {
        super(context, R.style.TransparentProgressDialog);
        setContentView(R.layout.transparent_dialog);
        mProgressCircle = findViewById(R.id.imageView);
    }

    public ProgressDialog(Context context, boolean isCancellable) {
        super(context, R.style.TransparentProgressDialog);
        setContentView(R.layout.transparent_dialog);
        setCancelable(isCancellable);
        mProgressCircle = findViewById(R.id.imageView);
    }

    @Override
    public void show() {
        rotateImage();
        super.show();
    }

    /**
     * rotates circle 360 degrees
     */
    private void rotateImage() {
        RotateAnimation anim = new RotateAnimation(0.0f, 360.0f,
                Animation.RELATIVE_TO_SELF, .5f,
                Animation.RELATIVE_TO_SELF, .5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(500);
        mProgressCircle.setAnimation(anim);
        mProgressCircle.startAnimation(anim);
    }

}
